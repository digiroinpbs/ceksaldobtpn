package id.corechain

import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import com.google.gson.JsonParser
import org.apache.commons.codec.digest.DigestUtils
import org.openqa.selenium.By
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.support.ui.WebDriverWait
import spark.Spark.*
import java.util.*
import redis.clients.jedis.Jedis
import spark.ModelAndView
import spark.Request
import spark.Response
import spark.TemplateViewRoute
import spark.template.thymeleaf.ThymeleafTemplateEngine
import java.io.BufferedWriter
import java.io.FileWriter
import javax.lang.model.util.Elements


class WebDriver(){
    fun getWebDriver(): WebDriver? {
        val props = Properties()
        props.load(this.javaClass.getResourceAsStream("/path.properties"))
        var driver: WebDriver
        System.setProperty("webdriver.chrome.driver", props.get("webdriver.chrome.driver").toString())
        driver = ChromeDriver()
        return driver
    }
}

fun getSaldo(req: Request, res: Response): ModelAndView {
    val jedis = Jedis("104.131.106.90",6379)
    jedis.auth("LCbpzEw2Zx5BNJaa63v9ivrSUIWzzjv9")
    jedis.select(0)
    var inquiry:String =jedis.get(req.params(":username"))
    val params = HashMap<String, Any>()
    val json: JsonObject
    val result:String
    val parser: JsonParser = JsonParser()
    try {
        json = parser.parse(inquiry) as JsonObject
        params.put("balance", json.get("amount").asString)
        params.put("bank",json.get("bank").asString)
        val timestamp:Long =json.get("lastupdate").asLong
        val time = java.util.Date(timestamp * 1000)
        params.put("update",time)
    }catch (e:Exception){
       e.printStackTrace()
    }
    params.put("account", req.params(":username"))
    return ModelAndView(params, "dashboard")
}

fun main(args: Array<String>) {
    port(7015)
    post("/checkSaldo") { req, res ->
        val json: JsonObject
        val result:String
        val parser: JsonParser = JsonParser()
        try {
            json = parser.parse(req.body()) as JsonObject
            result= checkSaldo(json.get("username").asString,json.get("password").asString)
        }catch (e:JsonParseException){
            e.printStackTrace()
            res.status(400)
            return@post "{invalid request}"
        }catch (e:TimeoutException){
            res.status(401)
            return@post "{time out}"
        }
        catch (e:Exception){
            e.printStackTrace()
            res.status(500)
            return@post "{system error}"
        }
        res.status(200)
        return@post result
    }
    get("/dashboard/:username", TemplateViewRoute { req, res -> getSaldo(req, res) }, ThymeleafTemplateEngine())
    post("/checkMutasi") { req, res ->

        val json: JsonObject
        val result:String
        val parser: JsonParser = JsonParser()
        try {
            json = parser.parse(req.body()) as JsonObject
            result= checkMutasi(json.get("username").asString,json.get("password").asString)
        }catch (e:Exception){
            e.printStackTrace()
            res.status(400)
            return@post "{invalid request}"
        }
        res.status(200)
        return@post result
    }
}

fun checkSaldo(username:String, password:String):String{
    val jedis = Jedis("104.131.106.90",6379)
    jedis.auth("LCbpzEw2Zx5BNJaa63v9ivrSUIWzzjv9")
    jedis.select(0)
    var result =""
    val webDriver =id.corechain.WebDriver().getWebDriver()
    val filename = "logCheckBalanceBTPN.txt"
    if(webDriver!=null){
        webDriver.manage().window().maximize();
        webDriver.get("https://sinaya.btpn.com/ib/ind/login")
        var element = webDriver.findElement(By.name("kaptchafield"))
        val wait = WebDriverWait(webDriver, 100000)
        wait.until(ExpectedConditions.attributeToBeNotEmpty(element,"value"));
        element = webDriver.findElement(By.name("username"))
        element.sendKeys(username)
        element = webDriver.findElement(By.name("password"))
        element.sendKeys(password)

        webDriver.findElement(By.xpath("//input[@type='submit']")).click()

        webDriver.findElement(By.xpath("//h3[contains(text(),'Informasi Rekening')]")).click()
        webDriver.findElement(By.linkText("Portofolio Rekening")).click()

        var table_element = webDriver.findElement(By.xpath("(//table//tbody//tr//td[@class='tx_right'])[2]"))
        var saldo =table_element.text
        saldo = saldo.replace(".",":")
        saldo = saldo.replace(",",".")
        saldo = saldo.replace(":",",")
        result=result+"{\"bank\":\"BTPN\",\"amount\":\""+saldo+"\",\"lastupdate\":\""+System.currentTimeMillis() / 1000L+"\"}"
        jedis.set(username+":BTPN",result)
        webDriver.findElement(By.linkText("Keluar")).click()

        var bw: BufferedWriter? = null
        var fw: FileWriter? = null
        try {
            var textResult = result.replace("{","")
            textResult = textResult.replace("}","")
            val content = textResult + "\n"
            fw = FileWriter(filename, true)
            bw = BufferedWriter(fw)
            bw.write(content)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                if (bw != null)
                    bw.close()
                if (fw != null)
                    fw.close()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }

        jedis.quit()
        webDriver.quit()
    }
    return result;
}

fun checkMutasi(username:String, password:String):String{
    val jedis = Jedis("104.131.106.90",6379)
    jedis.auth("LCbpzEw2Zx5BNJaa63v9ivrSUIWzzjv9")
    jedis.select(0)
    var result ="result :["
    val webDriver =id.corechain.WebDriver().getWebDriver()
    val filename = "logMutasiRekeningBTPN.txt"
    var bw: BufferedWriter? = null
    var fw: FileWriter? = null
    if(webDriver!=null){
        webDriver.manage().window().maximize();
        webDriver.get("https://sinaya.btpn.com/ib/ind/login")
        var element = webDriver.findElement(By.name("kaptchafield"))
        var wait = WebDriverWait(webDriver, 100000)
        wait.until(ExpectedConditions.attributeToBeNotEmpty(element,"value"));
        element = webDriver.findElement(By.name("username"))
        element.sendKeys(username)
        element = webDriver.findElement(By.name("password"))
        element.sendKeys(password)

        webDriver.findElement(By.xpath("//input[@type='submit']")).click()

        webDriver.findElement(By.xpath("//h3[contains(text(),'Informasi Rekening')]")).click()
        wait = WebDriverWait(webDriver, 100000)
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Mutasi Rekening")));
        webDriver.findElement(By.linkText("Mutasi Rekening")).click()
        webDriver.findElement(By.xpath("//input[@value='Lanjutkan']")).click()
        var tr_collection:List<WebElement>
        try {
            tr_collection = webDriver.findElements(By.xpath("//div[@class='table_grid']//table//tbody//tr"))
        }catch (e:Exception){
            result =result+"]"
            return  result
        }

        var row_num: Int
        var col_num: Int
        row_num = 1
        var first =true;
        var counter=0
        for (trElement in tr_collection) {
            val td_collection = trElement.findElements(By.xpath("td"))
            if(!first){
                result=result+","
            }
            first=false
            col_num = 1
            var res=""
            var indicator:String=""
            for (tdElement in td_collection) {
                if(td_collection.size==1){
                    continue
                }
                if(col_num==1){
                    res=res+"{\"Tanggal\":\""+tdElement.text+"\","
                }else if(col_num==2){
                    res=res+"\"Keterangan Transaksi\":\""+tdElement.text+"\","
                }else if(col_num==3){
                    res=res+"\"Debet\":\""+tdElement.text+"\","
                }else if(col_num==4){
                    res=res+"\"Kredit\":\""+tdElement.text+"\","
                }else if(col_num==5){
                    res=res+"\"Saldo\":\""+tdElement.text+"\"}"
                }
                col_num++
            }

            if(jedis.get(username+":BTPN:"+ sha1converter(res))==null && !res.equals("")){

                jedis.set(username+":BTPN:"+ sha1converter(res),res)
                try {
                    var textResult = res.replace("{","")
                    textResult = textResult.replace("}","")
                    val content = textResult + "\n"
                    fw = FileWriter(filename, true)
                    bw = BufferedWriter(fw)
                    bw.write(content)
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    try {
                        if (bw != null)
                            bw.close()
                        if (fw != null)
                            fw.close()
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            }

            result=result+res;
            row_num++
            counter++
        }
        result =result+"]"
        webDriver.findElement(By.linkText("Keluar")).click()

        jedis.quit()
        webDriver.quit()
    }
    return result;

}

fun sha1converter(param: String): String {
    return DigestUtils.sha1Hex(param)
}
